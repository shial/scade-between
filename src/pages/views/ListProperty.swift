#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class ListProperty: EObject {	
	@objc dynamic var id_l: String?
	@objc dynamic var userid_l: String?
	@objc dynamic var title_l: String?
	@objc dynamic var details_l: String?
	@objc dynamic var updated_l: String? 
	@objc dynamic var url_l: String? 
	@objc dynamic var isEnabled_l: Bool 
	
	@objc dynamic var id_r: String?
	@objc dynamic var userid_r: String?
	@objc dynamic var title_r: String?
	@objc dynamic var details_r: String?
	@objc dynamic var updated_r: String? 
	@objc dynamic var url_r: String?
	@objc dynamic var isEnabled_r: Bool 
		
	init(left leftList: List?, right rightList: List?) {
		self.id_l = leftList?.id
		self.userid_l = leftList?.userid
		self.title_l = leftList?.title
		self.details_l = leftList?.description
		self.updated_l = leftList?.updatedat?.description
		self.isEnabled_l = leftList != nil
		
		self.id_r = rightList?.id
		self.userid_r = rightList?.userid
		self.title_r = rightList?.title
		self.details_r = rightList?.description
		self.updated_r = rightList?.updatedat?.description
		self.isEnabled_r = rightList != nil
	}
}
