#if os(Linux) || os(Android)
	import SwiftFoundation
#endif
import ScadeKit

class BoardPageAdapter: SCDLatticePageAdapter {
	weak var homePage: HomePageAdapter?
	lazy var listControl = self.page!.getWidgetByName("listview") as! SCDWidgetsList
	lazy var addButton = self.page?.getWidgetByName("customwidget1") as? SCDWidgetsCustomWidget
	lazy var svgGroupAddList: SCDSvgGroup? = addButton?.drawing?.find(byId: "addButton") as? SCDSvgGroup
	lazy var svgRectAddList: SCDSvgRect? = addButton?.drawing?.find(byId: "addButtonRect") as? SCDSvgRect
	
	@objc dynamic var lists: [ListProperty] = []  
	
	lazy var lastPage: Int = -1
	
	init(_ home: HomePageAdapter) {
		super.init()
		self.homePage = home
	}
	
	override func load(_ path: String) {		
		super.load(path)
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
		svgGroupAddList?.gestureRecognizers.append(SCDSvgPanGestureRecognizer(handler: panGesture))
		listControl.onItemSelected.append(SCDWidgetsItemSelectedEventHandler(handler: self.rowClicked))
		
	}
	
	override func show(_ view: SCDLatticeView?, data: Any?) {
		super.show(view, data: data)
		print("show page \(self.page?.name ?? "")")
		homePage?.navigationBar?.titleLabel?.text = self.page?.name ?? ""
		if lastPage < 0 {
			lastPage = 0
			loadLists()
		}
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
		print("enterPage \(self.page?.name ?? "")")
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {
		print("exitPage \(self.page?.name ?? "")")
	}
	
	func panGesture(handler: SCDSvgGestureRecognizer?) {
		guard let handler = handler as? SCDSvgPanGestureRecognizer else { return }
		switch handler.state {
			case .changed:
				svgGroupAddList?.matrix.translateY += handler.deltaY
				svgRectAddList?.height.value += handler.deltaY
			case .ended:
				print("ended")
			default:
				print("\(handler.state)")
		}
	}
	
	func rowClicked(_ event: SCDWidgetsItemEvent?) {
		if let property = event?.item as? ListProperty {
			self.push(page: "list", data: property)
		}
	}
	
	func loadLists() {
		self.homePage?.backClosure?("showLoader")
		List.getLists(page: lastPage, success: {[weak self] lists in
			guard let restLists = lists else { return }
			self?.lastPage += restLists.count > 0 ? 1 : 0
			var previous: List? = nil
			for i in 0 ..< restLists.count {
				if i % 2 == 0 {
					self?.lists.append(ListProperty(left: previous, right: restLists[i]))
					previous = nil
				} else {
					previous = restLists[i]
				}
			}
			if let last = previous {
				self?.lists.append(ListProperty(left: last, right: nil))
			}
			self?.homePage?.backClosure?("hideLoader")
		}, failure: {[weak self] status, error in
			self?.homePage?.backClosure?("hideLoader")
			print("error getting lists for page: \(self?.lastPage ?? -1) with status: \(status) error: \(String(describing: error))")
		})
	}
}
