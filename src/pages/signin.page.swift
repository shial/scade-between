#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class SigninPageAdapter: SCDLatticePageAdapter {
	lazy var lbError = self.page!.getWidgetByName("labelError") as! SCDWidgetsLabel
	lazy var tbEmail = self.page!.getWidgetByName("tbEmail") as! SCDWidgetsTextbox
	lazy var tbPassword = self.page!.getWidgetByName("tfPassword") as! SCDWidgetsPasswordField
	lazy var bSignin = self.page!.getWidgetByName("bSignin") as! SCDWidgetsButton
	lazy var bSignup = self.page!.getWidgetByName("bSignup") as! SCDWidgetsButton
	lazy var bResetPassword = self.page!.getWidgetByName("bResetPassword") as! SCDWidgetsButton
	
	var callback: (MainClosure)?
	var isSignin: Bool = true {
		didSet {
			displayError("")
			if isSignin {
				setupForSignin()
			} else {
				setupForSignup()
			}
		}
	}
	
	init(_ callback: @escaping MainClosure) {
		super.init()
		self.callback = callback
	}
	
	override func load(_ path: String) {		
		super.load(path)
		tbEmail.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.textChange))
        tbPassword.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.textChange))
        bSignin.onClick.append(SCDWidgetsEventHandler(handler: self.signinAction))
        bSignup.onClick.append(SCDWidgetsEventHandler(handler: self.signupAction))
        bResetPassword.onClick.append(SCDWidgetsEventHandler(handler: self.passwordResetAction))
	}

	func displayError(_ message: String, color: SCDGraphicsRGB = SCDColor.red) {
		DispatchQueue.main.async(){
			self.lbError.text = message
			(self.lbError.getStyle(SCDWidgetsFontStyle().eClass()) as? SCDWidgetsFontStyle)?.color = color
			self.tbEmail.text = ""
			self.tbPassword.text = ""	
		}
	}
	
	func setupForSignin() {
		DispatchQueue.main.async(){
			self.bSignin.text = "Sign in"
			self.bSignup.text = "Sign up"
			self.bResetPassword.isVisible = true
		}
	}
	
	func setupForSignup() {
		DispatchQueue.main.async(){
			self.bSignin.text = "Sign up"
			self.bSignup.text = "Sign in"
			self.bResetPassword.isVisible = false
		}
	}
	
	func textChange(_ event: SCDWidgetsTextChangeEvent?) {
		if lbError.text.count > 0 {
			lbError.text = ""
		}
    }
	
	func signinAction(_ event: SCDWidgetsEvent?) {
		if isSignin {
			User.signin(email: tbEmail.text.lowercased(), password: tbPassword.text, success: {[weak self] token in
				User.accessToken = token?.value
				print(String(describing: token))
				self?.callback?(nil)
			}, failure: {[weak self] (status, error) in
				print("status: \(status) error: \(String(describing: error))")
				self?.displayError("Unable to login with given credentials.")
			})
		} else {
			User.signup(email: tbEmail.text.lowercased(), password: tbPassword.text, success: {[weak self] message in
				self?.isSignin = true
				self?.displayError(message?.message ?? "Account created!", color: SCDColor.green)
			}, failure: {[weak self] (status, error) in
				print("status: \(status) error: \(String(describing: error))")
				switch status {
					case 400:
						self?.displayError("Password is not valid. Minimum 8 and Maximum 64 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character @$^!%*?&#")
					case 409:
						self?.displayError("User with this email already exist.")
					default: 
						self?.displayError("Something went wrong. Please verify all the inputs fields and try again.")
				}
				
			})
		}
		tbEmail.text = ""
		tbPassword.text = ""
    }
	
	func signupAction(_ event: SCDWidgetsEvent?) {
		isSignin = !isSignin
    }
	
	func passwordResetAction(_ event: SCDWidgetsEvent?) {
		User.resetPassword(email: tbEmail.text.lowercased(), success: {[weak self] in
				self?.displayError("Reset password email sent.", color: SCDColor.green)
			}, failure: {[weak self] (status, error) in
				switch status {
					case 400:
						self?.displayError("Wrong email address")
					case 404:
						self?.displayError("Account with this email not found")
					default: 
						self?.displayError("Something went wrong. Please verify all the email field and try again.")
				}
			})
		tbEmail.text = ""
		tbPassword.text = ""
    }
}
