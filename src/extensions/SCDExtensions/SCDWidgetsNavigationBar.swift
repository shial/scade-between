#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

extension SCDWidgetsNavigationBar {
	var titleLabel: SCDWidgetsLabel? {
		return self.getWidgetByName("title1") as? SCDWidgetsLabel
	}
	
	var backButton: SCDWidgetsButton? {
		return self.getWidgetByName("backbutton1") as? SCDWidgetsButton
	}
	
	var extraButton: SCDWidgetsButton? {
		return self.getWidgetByName("extrabutton1") as? SCDWidgetsButton
	}
}
