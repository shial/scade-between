#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class BestsPageAdapter: SCDLatticePageAdapter {
	weak var homePage: HomePageAdapter?
	var parentName: String?
	
	init(_ home: HomePageAdapter) {
		super.init()
		self.homePage = home
	}
	
	override func load(_ path: String) {		
		super.load(path)
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: self.enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: self.exitPage))
	}
	
	func enterPage(event: SCDWidgetsEnterEvent?) {
		print("enterPage \(self.page?.name ?? "")")
		homePage?.navigationBar?.titleLabel?.text = self.page?.name ?? ""
	}
	
	func exitPage(event: SCDWidgetsExitEvent?) {
		print("exitPage \(self.page?.name ?? "")")
	}
}
