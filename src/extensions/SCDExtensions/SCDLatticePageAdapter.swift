#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

extension SCDLatticePageAdapter {
	func push(page: String, data: Any? = nil) {
		var info: [String:Any?] = ["parent":self.page?.name] 
		if let d = data {
			info["userInfo"] = d
		}
		navigation?.go(with: "pages/\(page).page", data: info, transition: "FORWARD_PUSH")
	}
	
	func unwind(to page: String,  data: Any? = nil) {
		var info: [String:Any?] = ["unwind":self.page?.name]
		if let d = data {
			info["userInfo"] = d
		}
		navigation?.go(with: "pages/\(page).page", data: info, transition: "BACKWARD_PUSH")
	}
}
