#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

extension SCDWidgetsWidget {
	var isExclude: Bool {
		get {
			return (self.layoutData as? SCDLayoutGridData)?.isExclude ?? false
		}
		set {
			if let gd = self.layoutData as? SCDLayoutGridData {
				gd.isExclude = newValue
				self.isVisible = !newValue
			}
		}
	}
}