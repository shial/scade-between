#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class ListPageAdapter: SCDLatticePageAdapter {
	weak var homePage: HomePageAdapter?
	var parentName: String?
	
	init(_ home: HomePageAdapter) {
		super.init()
		self.homePage = home
	}
	
	override func load(_ path: String) {		
		super.load(path)
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
	}
	
	override func show(_ view: SCDLatticeView?, data: Any?) {
		super.show(view, data: data)
		homePage?.toolBar?.isVisible = false
		homePage?.navigationBar?.titleLabel?.text = self.page?.name ?? ""
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
		print("enterPage \(self.page?.name ?? "")")
		if let userInfo = event?.data as? [String : Any] {
			parentName = userInfo["parent"] as? String
		}
		if let name = parentName {
			homePage?.navigationBar?.backButton?.isVisible = true
			homePage?.toolBar?.isExclude = true
			homePage?.navigationBar?.backButton?.onClick = [SCDWidgetsEventHandler{[weak self] _ in
				let data: [String:Any] = ["unwind":self?.page?.name ?? ""] 
				self?.navigation?.go(with: "pages/\(name).page", data: data, transition: "BACKWARD_PUSH")
			}]
		} else {
			homePage?.navigationBar?.backButton?.isVisible = false
		}
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {
		print("exitPage \(self.page?.name ?? "")")
		homePage?.toolBar?.isVisible = true
		homePage?.toolBar?.isExclude = false
		homePage?.navigationBar?.backButton?.isVisible = false
		homePage?.navigationBar?.backButton?.onClick = []
	}
}
