#if os(Linux) || os(Android)
	import SwiftFoundation
#endif
import ScadeKit

class SLNavigationPageAdapter: SCDLatticePageAdapter {
	weak var navigationBar: SCDWidgetsNavigationBar?
	weak var toolBar: SCDWidgetsToolBar?
	
	var shouldHideBottomBar: Bool { return false }
	var parentName: String?
	
	override func load(_ path: String) {		
		super.load(path)
		print("SLNavigationPageAdapter load: \(self.page?.name ?? "-")")
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
	}
	
	override func activate(_ view: SCDLatticeView?) {
		super.activate(view)
		print("SLNavigationPageAdapter activate: \(self.page?.name ?? "-")")
		toolBar?.isVisible = !shouldHideBottomBar
		navigationBar?.titleLabel?.text = self.page?.name ?? ""
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
		if let userInfo = event?.data as? [String : Any] {
			print("SLNavigationPageAdapter enterPage \(self.page?.name ?? "-")")
			print(userInfo)
			parentName = userInfo["parentName"] as? String
			navigationBar = userInfo["navigationBar"] as? SCDWidgetsNavigationBar
			toolBar = userInfo["toolBar"] as? SCDWidgetsToolBar
		}
		if let name = parentName {
			navigationBar?.backButton?.isVisible = true
			navigationBar?.backButton?.onClick = [SCDWidgetsEventHandler{[weak self] _ in
				let data: [String:Any] = ["unwind":self?.page?.name ?? ""] 
				self?.navigation?.go(with: "pages/\(name).page", data: data, transition: "BACKWARD_PUSH")
			}]
		} else {
			navigationBar?.backButton?.isVisible = false
		}
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {
		print("SLNavigationPageAdapter exitPage: \(self.page?.name ?? "-")")
		navigationBar?.backButton?.onClick = []
	}
}
