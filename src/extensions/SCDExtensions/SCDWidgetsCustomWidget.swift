#if os(Linux) || os(Android)
	import SwiftFoundation
#endif
import ScadeKit

extension SCDWidgetsCustomWidget {
	func animateTopEdgeMove(moveY: Float, duration: Float) {
		guard let id = self.drawing?.id else { return }
		let svgGroupAddList: SCDSvgGroup? = drawing?.find(byId: id) as? SCDSvgGroup
		let svgRectAddList: SCDSvgRect? = drawing?.find(byId: id + "Rect") as? SCDSvgRect

		let move = SCDSvgBaseAnimation.getMoveByAnimation(dy: moveY, duration: duration)
		let height = Float(svgRectAddList?.height.value ?? 0)
		let size = SCDSvgBaseAnimation.getPropertyAnimation(.height, from: height, to: height - moveY, duration: duration)
		svgRectAddList?.animations.append(size)
		svgGroupAddList?.animations.append(move)
	}
}
