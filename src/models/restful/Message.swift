#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import Foundation

class Message: Decodable {
	var code: Int!
    var type: String!
    var message: String!
}
