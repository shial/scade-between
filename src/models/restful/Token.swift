#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import Foundation

class Token: Decodable {
    var userid: String!
    var value: String!
    var valid: Date!
    var type: String!
}
