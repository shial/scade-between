#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class List: Decodable {
	var id: String
	var userid: String
	var createdat: Date?
	var updatedat: Date?
	var isprivate: Bool
	var title: String
	var description: String
	
	public struct PathPattern {
        static var listById: String { return "/api/list/:listid" }
        static var listRoute: String { return "/api/list" }
        static var likeList: String { return "/api/like/list/:list" }
    }
    
	init() {
		id = "id"
		userid = "userid"
		isprivate = false
		title = "title"
		description = "description"
	}
	
	class func updateLikeListStatus(listid: String, success: ((Message?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) -> URLSessionDataTask? {
        return Message.get(config: APILazeConfiguration.self, path: PathPattern.likeList.patternToPath(with: ["list":listid])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func getList(with id: String, success: ((List?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = List.get(config: APILazeConfiguration.self, path: PathPattern.listById.patternToPath(with: ["listid":id])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func getLists(page: Int, success: (([List]?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = [List].get(config: APILazeConfiguration.self, path: PathPattern.listRoute.patternToPath(), queryItems: [URLQueryItem(name: "page", value: "\(page)")]) { (response, result, error)  in
        	guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func createOnServer(title: String, description: String, isprivate: Bool, success: ((List?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let body = """
        {
            "title":"\(title)",
            "description":"\(description)"
            "isPrivate": "\(isprivate)"
        }
        """
        let _ = List.post(config: APILazeConfiguration.self, path: PathPattern.listRoute.patternToPath(), body: body, queryItems: nil) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
    func updateOnServer(success: ((List?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let body = """
        {
            "title":"\(self.title)",
            "description":"\(self.description)"
            "isPrivate": "\(self.isprivate)"
        }
        """
        let _ = List.put(config: APILazeConfiguration.self, path: PathPattern.listById.patternToPath(with: ["listid":self.id]), body: body, queryItems: nil) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
    
    func deleteOnServer(success: (() ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = SLazeKit<APILazeConfiguration>.delete(path: PathPattern.listById.patternToPath(with: ["listid":self.id])) { (response, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?()
        }
    }
}