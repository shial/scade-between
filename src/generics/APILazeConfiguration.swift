#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import Foundation

public class APILazeConfiguration: LazeConfiguration {
	public class var basePath: String? {
        return "http://between.sytes.net"
    }
    
    public class var basePort: Int? {
        return nil
    }
    
    public class var token: String {
        return User.accessToken ?? ""
    }
    
    public class var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        return decoder
    }
    
    public class var urlSession: URLSession {
        return URLSession.shared
    }
    
    public class func setup(_ request: URLRequest) -> URLRequest? {
        var request: URLRequest = request
        if !token.isEmpty {
            request.setValue("Basic \(token)", forHTTPHeaderField: "Authorization")
        }
        request.setValue(UUID().uuidString.lowercased(), forHTTPHeaderField: "x-request-id")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    public class func handle(_ response: HTTPURLResponse?) {
        if response?.statusCode == 401 {
            
        }
    }
    
    public static func synchronize(_ obj: Any) {
        
    }
}
