#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class HomePageAdapter: SCDLatticePageAdapter {
	lazy var navigationBar = self.page?.getWidgetByName("navbar1") as? SCDWidgetsNavigationBar
	lazy var toolBar = self.page?.getWidgetByName("toolbar1") as? SCDWidgetsToolBar
	lazy var pageContainer = self.page?.getWidgetByName("pagecontainer1") as? SCDLatticePageContainer
	
	var backClosure: (MainClosure)?
	
	init(_ callback: @escaping MainClosure) {
		super.init()
		self.backClosure = callback
	}
	
	lazy var bestsPage : BestsPageAdapter = BestsPageAdapter(self)
	lazy var boardPage : BoardPageAdapter = BoardPageAdapter(self)
	lazy var listPage : ListPageAdapter = ListPageAdapter(self)
	lazy var profilePage : ProfilePageAdapter = ProfilePageAdapter(self)
	
	// page adapter initialization
	override func load(_ path: String) {		
		super.load(path)
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
		// pages could either be loaded on demand
		// or in advance. We load all pages in advance here
		self.bestsPage.load("pages/bests.page")
		self.boardPage.load("pages/board.page")
		self.listPage.load("pages/list.page")
		self.profilePage.load("pages/profile.page")
		
		// lets add event handlers
		self.toolBar?.itemBy(name: "bests")?.onClick.append(SCDWidgetsEventHandler{ _ in self.showPage(self.bestsPage)})
		self.toolBar?.itemBy(name: "board")?.onClick.append(SCDWidgetsEventHandler{ _ in self.showPage(self.boardPage)})
		self.toolBar?.itemBy(name: "profile")?.onClick.append(SCDWidgetsEventHandler{ _ in self.showPage(self.profilePage)})
		
		// Finally, we use the page container to show page1
		self.bestsPage.show(self.pageContainer)
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {		
	}
	
	func showPage(_ page:SCDLatticePageAdapter) {
		if let adapter = self.pageContainer?.page?.adapter {
			switch adapter {
				case is BestsPageAdapter:
					bestsPage.exitPage(event: nil)
				case is ProfilePageAdapter:
					profilePage.exitPage(nil)
				default: 
					break
			}
		}
		page.show(self.pageContainer)
	}
}
