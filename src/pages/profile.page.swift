import ScadeKit

class ProfilePageAdapter: SCDLatticePageAdapter {
	weak var homePage: HomePageAdapter?
	lazy var tbUserName = self.page!.getWidgetByName("textusername") as! SCDWidgetsTextbox
	lazy var tbUserDescription = self.page!.getWidgetByName("textuserdetails") as! SCDWidgetsTextbox
	lazy var bLogout = self.page!.getWidgetByName("bLogout") as! SCDWidgetsButton
	lazy var bFollowing = self.page!.getWidgetByName("bFollowing") as! SCDWidgetsButton
	lazy var bFollowers = self.page!.getWidgetByName("bFollowers") as! SCDWidgetsButton
	
	var followersTask: URLSessionDataTask? 
	var followingTask: URLSessionDataTask?
	
	var maxDescriptionSize: Int = 69
	
	init(_ home: HomePageAdapter) {
		super.init()
		self.homePage = home
	}
	
	override func load(_ path: String) {		
		super.load(path)
		tbUserName.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.textChange))
		tbUserDescription.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.textChange))
        bLogout.onClick.append(SCDWidgetsEventHandler(handler: self.logoutAction))
        
        self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
		print("enterPage \(self.page?.name ?? "")")
		homePage?.navigationBar?.extraButton?.text = "save"
		homePage?.navigationBar?.extraButton?.onClick = [SCDWidgetsEventHandler{[weak self] _ in
			if let strong = self, let current = User.currentUser {
				self?.homePage?.backClosure?("showLoader")
				current.name = strong.tbUserName.text
				current.details = strong.tbUserDescription.text
				current.updateOnServer(success: { user in
					DispatchQueue.main.async(){
						self?.homePage?.navigationBar?.extraButton?.isVisible = false
						self?.homePage?.backClosure?("hideLoader")
					}
				},  failure: { status, error in
					DispatchQueue.main.async(){
						self?.homePage?.navigationBar?.extraButton?.isVisible = false
						self?.homePage?.backClosure?("hideLoader")
					}
					print("Error updating user:\(current.id ?? "") with status: \(status) error: \(String(describing: error))")
				})
			}
		}]
		homePage?.navigationBar?.titleLabel?.text = self.page?.name ?? ""
		tbUserName.text = User.currentUser?.name ?? ""
		tbUserDescription.text = User.currentUser?.details ?? ""
		bFollowing.text = ""
		bFollowers.text = ""
		if let userid = User.currentUser?.id {
			featchUserData(userid)
		}
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {
		print("exitPage \(self.page?.name ?? "")")
		cancelFeatch()
		homePage?.navigationBar?.extraButton?.isVisible = false
		homePage?.navigationBar?.extraButton?.onClick = []
	}
	
	func textChange(_ event: SCDWidgetsTextChangeEvent?) {
		homePage?.navigationBar?.extraButton?.isVisible = true
		if tbUserDescription.text.count > maxDescriptionSize {
			tbUserDescription.text.removeLast(tbUserDescription.text.count - maxDescriptionSize)
		}
    }
	
	func cancelFeatch() {
		[followersTask, followingTask].forEach({ $0?.cancel() })
	}
	
	func featchUserData(_ userid: String) {
		followersTask = User.getFollowersCount(userid: userid, success: {[weak self] response in
			if let count = response?.value {
				DispatchQueue.main.async(){self?.bFollowers.text = "\(count)"}
			}
		}, failure: { status, error in 
			print("Error featch User:\(userid) Data with status: \(status) error: \(String(describing: error))")
		})
		followingTask = User.getFollowingCount(userid: userid, success: {[weak self] response in
			if let count = response?.value {
				DispatchQueue.main.async(){self?.bFollowing.text = "\(count)"}
			}
		}, failure: { status, error in 
			print("Error featch User:\(userid) Data with status: \(status) error: \(String(describing: error))")
		})
	}
	
	func logoutAction(_ event: SCDWidgetsEvent?) {
		homePage?.backClosure?("showLoader")
		User.logout(success: {[weak self] in
			self?.localLogout()
		}, failure: {[weak self] status, error in
			self?.localLogout()
		})
    }
	
	func localLogout() {
		SCDRuntime.saveFile("token", content: "")
		User.accessToken = ""
		homePage?.backClosure?(nil)
	}
}
