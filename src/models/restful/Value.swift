#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import Foundation
import ScadeKit

struct Value: Decodable {
	public var value: Int64?
}
