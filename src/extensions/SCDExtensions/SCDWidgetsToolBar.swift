#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

extension SCDWidgetsToolBar {
	func itemBy(name: String) -> SCDWidgetsClickable? {
		return self.getWidgetByName(name) as? SCDWidgetsClickable
	}
}
