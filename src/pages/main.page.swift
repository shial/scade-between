#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

typealias MainClosure = (_ action: String?) -> ()

class MainPageAdapter: SCDLatticePageAdapter {
	lazy var pageContainer = self.page?.getWidgetByName("pagecontainer1") as? SCDLatticePageContainer
	lazy var spinningCircles = self.page?.getWidgetByName("spinningCircles") as? SCDWidgetsCustomWidget
	
	lazy var pageHome : HomePageAdapter = HomePageAdapter({[weak self] action in
		guard let strong = self else { return }
		guard let action = action else {
			print("Missing action - will proceed with presentFlow")
			DispatchQueue.main.async(){
				strong.presentFlow()
			}
			return
		}
		print("Will proceed with action: \(action)")
		switch action {
			case "showLoader":
				DispatchQueue.main.async(){
				strong.showLoader()
			}
			case "hideLoader":
				DispatchQueue.main.async(){
				strong.dismissLoader()
			}
			default:
				break
		}
	})
	lazy var pageSingin : SigninPageAdapter = SigninPageAdapter({[weak self] action in
		guard let strong = self else { return }
		guard let action = action else {
			strong.presentFlow()
			return
		}
		switch action {
			default:
				break
		}
	})
	
	override func load(_ path: String) {		
		super.load(path)
		
		pageHome.load("pages/home.page")
		pageSingin.load("pages/signin.page")
		
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
	}
	
	override func show(_ view: SCDLatticeView?) {
		super.show(view)
		self.setupSpinningCircle()
	}
	
	func enterPage(event: SCDWidgetsEnterEvent?) {
		showLoader()
		User.accessToken = {
		    let filePath = SCDRuntime.getSystem().path(forDocument: "token")
			guard let token = SCDRuntime.loadFile(filePath),
			token.count > 0 else { 
				return nil
			}
			return token
		}() 
		presentFlow()
	}
	
	func exitPage(event: SCDWidgetsExitEvent?) {
	}
	
	func presentFlow() {
		if let token = User.accessToken, token.count > 0 {
			User.getUser(success: {[weak self] user in
				guard let strong = self else { return }
				User.currentUser = user
				DispatchQueue.main.async(){
					strong.showPage(strong.pageHome)
				}
			}, failure: {[weak self] status, error in
				guard let strong = self else { return }
				DispatchQueue.main.async(){
					strong.showPage(strong.pageHome)
				}
			})
		} else {
			showPage(pageSingin)
		}
	}
	
	func showPage(_ page:SCDLatticePageAdapter) {
		page.show(self.pageContainer)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
			self.dismissLoader()
		})
	}
	
	func showLoader() {
		pageContainer?.isExclude = true
		spinningCircles?.isExclude = false
	}
	
	func dismissLoader() {
		pageContainer?.isExclude = false
		spinningCircles?.isExclude = true
	}
	
	func setupSpinningCircle() {
		let circleAnimationValues : [String:[Int]] = [
		          "c11" : [1,0,0,0,0,0,0,0],
		          "c12" : [0,1,0,0,0,0,0,0],
		          "c13" : [0,0,1,0,0,0,0,0],
		          "c14" : [0,0,0,1,0,0,0,0],
		          "c15" : [0,0,0,0,1,0,0,0],
		          "c16" : [0,0,0,0,0,1,0,0],
		          "c17" : [0,0,0,0,0,0,1,0],
		          "c18" : [0,0,0,0,0,0,0,1]
		      ]
		  
		for (id,values) in circleAnimationValues {
			if let circle = self.page?.drawing?.find(byId: id) as? SCDSvgCircle {
				let opacityAnimation = SCDSvgPropertyAnimation("fillOpacity", values: values)
				opacityAnimation.duration = 1.3
				opacityAnimation.repeatCount = -1
				circle.animations.append(opacityAnimation)
			}
		}
	}
}
