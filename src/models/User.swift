#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import Foundation
import ScadeKit

class User: Decodable {
	public let id: String?
    public let email: String?
    public var name: String?
    public var details: String?
    public var type: String?
    
    public static var currentUser: User?
	public static var accessToken: String? {
		didSet {
			let filePath = SCDRuntime.getSystem().path(forDocument: "token") 
			SCDRuntime.saveFile(filePath, content: accessToken ?? "")
		}
	}
	
	public struct PathPattern {
        static var signin: String { return "/admin/login" }
        static var signup: String { return "/admin/signup" }
        static var logout: String { return "/admin/logout" }
        static var reset: String { return "/admin/reset" }
        static var userRoute: String { return "/api/user" }
        static var followers: String { return "/followers/user/:userid" }
        static var following: String { return "/following/user/:userid" }
        static var follow: String { return "/api/follow/user/:userid" }
    }
	
	class func updateFololowStatus(userid: String, success: ((Message?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) -> URLSessionDataTask? {
        return Message.get(config: APILazeConfiguration.self, path: PathPattern.follow.patternToPath(with: ["userid":userid])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func getFollowingCount(userid: String, success: ((Value?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) -> URLSessionDataTask? {
        return Value.get(config: APILazeConfiguration.self, path: PathPattern.following.patternToPath(with: ["userid":userid])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func getFollowersCount(userid: String, success: ((Value?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) -> URLSessionDataTask? {
        return Value.get(config: APILazeConfiguration.self, path: PathPattern.followers.patternToPath(with: ["userid":userid])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	func updateOnServer(success: ((User?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let body = """
        {
            "name":"\(self.name ?? "")",
            "details":"\(self.details ?? "")"
        }
        """
        let _ = User.post(config: APILazeConfiguration.self, path: PathPattern.userRoute.patternToPath(), body: body, queryItems: nil) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
    
    func deleteOnServer(success: (() ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = SLazeKit<APILazeConfiguration>.delete(path: PathPattern.userRoute.patternToPath()) { (response, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?()
        }
    }
	
	class func getUser(success: ((User?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = User.get(config: APILazeConfiguration.self, path: PathPattern.userRoute.patternToPath()) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func signup(email: String, password: String, success: ((Message?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let body = """
        {
            "login":"\(email)",
            "password":"\(password)"
        }
        """
		let _ = Message.post(config: APILazeConfiguration.self, path: PathPattern.signup.patternToPath(), body: body) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func signin(email: String, password: String, success: ((Token?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let body = """
        {
            "login":"\(email)",
            "password":"\(password)"
        }
        """
        let _ = Token.post(config: APILazeConfiguration.self, path: PathPattern.signin.patternToPath(), body: body) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
	
	class func logout(success: (() ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = SLazeKit<APILazeConfiguration>.get(path: PathPattern.logout.patternToPath()) { (response, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?()
        }
    }
	
	class func resetPassword(email: String, success: (() ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) {
        let _ = SLazeKit<APILazeConfiguration>.get(path: PathPattern.reset.patternToPath(), queryItems: [URLQueryItem(name: "email", value: "\(email)")]) { (response, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?()
        }
    }
}
