#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class SLazeKitScade {
	class func configureDatabase() {
		let manager = FileManager.default
		var databasePath: String!
		#if os(iOS)
	    	let applicationDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
	    	databasePath = applicationDirectory.first! + "/slazekit.sqlite"
		    
		#endif
		#if os(Android)
			databasePath = "" + "/slazekit.sqlite"
		#endif
		if !manager.fileExists(atPath: databasePath) {
			manager.createFile(atPath: databasePath, contents: nil, attributes: nil)
		}
	}
}
