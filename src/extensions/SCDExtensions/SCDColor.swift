#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class SCDColor {
	class var red: SCDGraphicsRGB {
		return SCDGraphicsRGB(red:255, green: 0, blue:38)
	}
	class var green: SCDGraphicsRGB {
		return SCDGraphicsRGB(red:142, green:0, blue:250)
	}
	class var gray: SCDGraphicsRGB {
		return SCDGraphicsRGB(red: 211, green: 211, blue: 211)
	}
}
