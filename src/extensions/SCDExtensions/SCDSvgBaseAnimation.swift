#if os(Linux) || os(Android)
	import SwiftFoundation
#endif
import ScadeKit

enum SvgProperty {
	case x
	case y
	case width
	case height
	case fill
	case fillOpacity
	case stroke
	case strokeWidth
}

extension SCDSvgBaseAnimation {
	class func getMoveByAnimation(dx: Float = 0, dy: Float = 0, duration: Float) -> SCDSvgTranslateAnimation {
		let moveByAnimation = SCDSvgTranslateAnimation()
		moveByAnimation.dx = dx
	 	moveByAnimation.dy = dy
		moveByAnimation.duration = duration
		return moveByAnimation
 	}
	
	class func getPropertyAnimation(_ property: SvgProperty, from: Float, to: Float, duration: Float) -> SCDSvgPropertyAnimation {
		let strokeWidthAnimation = SCDSvgPropertyAnimation("\(property)",from:from,to:to)
		strokeWidthAnimation.duration = duration
		return strokeWidthAnimation
	}
	
	class func getGroupAnimation(delay: Float = 0, animations: SCDSvgBaseAnimation...) -> SCDSvgGroupAnimation {
		 let ga = SCDSvgGroupAnimation() 
		 ga.animations = animations
		 ga.delay = delay
		 return ga
	}
}
