import ScadeKit

class EditlistPageAdapter: SCDLatticePageAdapter {
	weak var homePage: HomePageAdapter?
	
	lazy var titleEditBox = self.page!.getWidgetByName("textbox1") as! SCDWidgetsTextbox
	lazy var descriptionEditBox = self.page!.getWidgetByName("textbox2") as! SCDWidgetsTextbox
	lazy var imageView = self.page!.getWidgetByName("bitmap1") as! SCDWidgetsBitmap
	lazy var checkbox = self.page!.getWidgetByName("checkbox1") as! SCDWidgetsCheckbox
	
	private var editList: List?
	
	init(_ home: HomePageAdapter) {
		super.init()
		self.homePage = home
	}
	
	override func load(_ path: String) {		
		super.load(path)
		self.page?.onEnter.append(SCDWidgetsEnterEventHandler(handler: enterPage))
		self.page?.onExit.append(SCDWidgetsExitEventHandler(handler: exitPage))
		
		titleEditBox.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.titleChange))
		descriptionEditBox.onTextChange.append(SCDWidgetsTextChangeEventHandler(handler: self.descriptionChange))
	}
	
	func enterPage(_ event: SCDWidgetsEnterEvent?) {
		print("enterPage \(self.page?.name ?? "")")
		homePage?.navigationBar?.extraButton?.text = "save"
		homePage?.navigationBar?.extraButton?.onClick = [SCDWidgetsEventHandler{[weak self] _ in
			if let strong = self, let _ = User.currentUser {
				strong.homePage?.backClosure?("showLoader")
				if let list = strong.editList {
					list.updateOnServer(success: {[weak self] list in
						self?.homePage?.backClosure?("hideLoader")
					}, failure: {[weak self] status, error in
						self?.homePage?.backClosure?("hideLoader")
					})
				} else {
					List.createOnServer(title: strong.titleEditBox.text, 
						description: strong.descriptionEditBox.text, 
						isprivate: strong.checkbox.isChecked, success: {[weak self] list in
						self?.homePage?.backClosure?("hideLoader")
					}, failure: {[weak self] status, error in
						self?.homePage?.backClosure?("hideLoader")
					})
				}
			}
		}]
		homePage?.navigationBar?.titleLabel?.text = self.page?.name ?? ""
	}
	
	func exitPage(_ event: SCDWidgetsExitEvent?) {
		print("exitPage \(self.page?.name ?? "")")
		homePage?.navigationBar?.extraButton?.isVisible = false
		homePage?.navigationBar?.extraButton?.onClick = []
	}
	
	func titleChange(_ event: SCDWidgetsTextChangeEvent?) {
		homePage?.navigationBar?.extraButton?.isVisible = true
    }
	
	func descriptionChange(_ event: SCDWidgetsTextChangeEvent?) {
		homePage?.navigationBar?.extraButton?.isVisible = true
    }
}
