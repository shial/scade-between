#if os(Linux) || os(Android)
import SwiftFoundation
#endif
import ScadeKit

class Wish: Decodable {
	public struct PathPattern {
        static var likeWish: String { return "/api/like/wish/:wish" }
    }
	
	class func updateLikeWishStatus(wishid: String, success: ((Message?) ->())? = nil, failure: ((_ statusCode: Int, _ error: Error?) ->())? = nil) -> URLSessionDataTask? {
        return Message.get(config: APILazeConfiguration.self, path: PathPattern.likeWish.patternToPath(with: ["wish":wishid])) { (response, result, error)  in
            guard let statusCode = response.http?.statusCode, 200..<300 ~= statusCode else {
                failure?(response.http?.statusCode ?? -1,error)
                return
            }
            success?(result)
        }
    }
}
